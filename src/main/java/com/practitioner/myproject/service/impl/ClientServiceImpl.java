package com.practitioner.myproject.service.impl;

import com.practitioner.myproject.model.Account;
import com.practitioner.myproject.model.Client;
import com.practitioner.myproject.service.AccountService;
import com.practitioner.myproject.service.ClientService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {

    public final Map<String, Client> clients = new ConcurrentHashMap<>();
    public AccountService accountService;

    @Override
    public void insertClient(Client client) {
        this.clients.put(client.document, client);
    }

    @Override
    public List<Client> listClients(int page, int size) {
        final List<Client> aux = this.clients.values().stream().collect(Collectors.toList());

        final int startIndex = page*size;
        final int lastIndex = startIndex + size;

        if (startIndex >= aux.size())
            return Collections.emptyList();
        else if (lastIndex >= aux.size())
            return aux.subList(startIndex, aux.size());
        else
            return aux.subList(startIndex, lastIndex);
    }

    @Override
    public Client getClient(String doc) {
        if (this.clients.containsKey(doc))
            return this.clients.get(doc);
        else
            throw new RuntimeException("No existe cliente");
    }

    @Override
    public void updateClient(Client client) {
        if (!this.clients.containsKey(client.document))
            throw new RuntimeException("No existe cliente");

        final List<String> acc = this.listClientAccounts(client.document);
        client.accounts = acc;
        this.clients.replace(client.document, client);
    }

    @Override
    public void patchClient(Client client) {
        final Client aux = this.clients.get(client.document);

        if (client.name != null)
            aux.name = client.name;
        if (client.age != aux.age)
            aux.age = client.age;
        if (client.phone != null)
            aux.phone = client.phone;
        if (client.email != null)
            aux.email = client.email;
        if (client.address != null)
            aux.address = client.address;

        this.clients.replace(aux.document, aux);
    }

    @Override
    public void deleteClient(String doc) {
        if (this.clients.containsKey(doc))
            this.clients.remove(doc);
        else
            throw new RuntimeException("No existe cliente");
    }

    @Override
    public void insertClientAccount(String document, String number_account) {
        this.getClient(document).accounts.add(number_account);
    }

    @Override
    public List<String> listClientAccounts(String document) {
        return this.getClient(document).accounts;
    }

    @Override
    public boolean IsClientAccount(String doc, String acc) {
        return this.getClient(doc).accounts.contains(acc);
    }
}
