package com.practitioner.myproject.service.impl;

import com.practitioner.myproject.model.Account;
import com.practitioner.myproject.service.AccountService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    public final Map<String, Account> accounts = new ConcurrentHashMap<>();

    @Override
    public void insertAccount(Account account) {
        this.accounts.put(account.number, account);
    }

    @Override
    public List<Account> listAccounts() {
        return this.accounts.values().stream().collect(Collectors.toList());
    }

    @Override
    public Account getAccount(String number) {
        if (this.accounts.containsKey(number))
            return this.accounts.get(number);
        else
            throw new RuntimeException("No existe cuenta");
    }

    @Override
    public void updateAccount(Account account) {
        if (this.accounts.containsKey(account.number))
            this.accounts.replace(account.number, account);
        else
            throw new RuntimeException("No existe cuenta");
    }

    @Override
    public void patchAccount(Account account) {
        final Account aux = this.accounts.get(account.number);

        if (account.currency != null)
            aux.currency = account.currency;
        if (account.amount != null)
            aux.amount = account.amount;
        if (account.type != null)
            aux.type = account.type;
        if (account.status != null)
            aux.status = account.status;
        if (account.office != null)
            aux.office = account.office;

        this.accounts.replace(aux.number, aux);
    }

    @Override
    public void deleteAccount(String number) {
        if (this.accounts.containsKey(number))
            this.accounts.remove(number);
        else
            throw new RuntimeException("No existe cuenta");
    }
}
