package com.practitioner.myproject.model;

public class Account {
    public String number;
    public String currency;
    public Double amount;
    public String type;
    public String status;
    public String office;
}
