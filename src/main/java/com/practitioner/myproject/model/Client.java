package com.practitioner.myproject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Client {
    public String document;
    public String name;
    public int age;
    public String phone;
    public String email;
    public String address;

    @JsonIgnore
    public List<String> accounts = new ArrayList<>();
}
